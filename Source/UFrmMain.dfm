object FrmMain: TFrmMain
  Left = 457
  Top = 200
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #25968#25454#24211#31649#29702#24037#20855
  ClientHeight = 416
  ClientWidth = 402
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Gauge2: TGauge
    Left = 8
    Top = 295
    Width = 81
    Height = 18
    BackColor = clCream
    BorderStyle = bsNone
    ForeColor = clBlue
    MaxValue = 22
    Progress = 0
  end
  object Button2: TButton
    Left = 232
    Top = 367
    Width = 62
    Height = 25
    Caption = #21021#22987#21270
    Enabled = False
    TabOrder = 2
    OnClick = Button2Click
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 369
    Height = 129
    Caption = #26381#21153#22120#20449#24687
    TabOrder = 0
    object Label1: TLabel
      Left = 21
      Top = 24
      Width = 84
      Height = 13
      Caption = #25968#25454#24211#26381#21153#22120#65306
    end
    object Label2: TLabel
      Left = 21
      Top = 52
      Width = 72
      Height = 13
      Caption = #30331#24405#29992#25143#21517#65306
    end
    object Label3: TLabel
      Left = 21
      Top = 78
      Width = 60
      Height = 13
      Caption = #30331#24405#23494#30721#65306
    end
    object Edit1: TEdit
      Left = 111
      Top = 21
      Width = 249
      Height = 21
      TabOrder = 0
      Text = '127.0.0.1'
    end
    object Edit2: TEdit
      Left = 111
      Top = 48
      Width = 249
      Height = 21
      TabOrder = 1
      Text = 'sa'
    end
    object Edit3: TEdit
      Left = 111
      Top = 75
      Width = 249
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
    end
    object Button1: TButton
      Left = 269
      Top = 101
      Width = 91
      Height = 25
      Caption = #38142#25509
      TabOrder = 3
      OnClick = Button1Click
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 143
    Width = 369
    Height = 146
    Caption = #25968#25454#24211
    TabOrder = 1
    object Label4: TLabel
      Left = 21
      Top = 122
      Width = 84
      Height = 13
      Caption = #36873#25321#25968#25454#24211#21517#65306
    end
    object Label5: TLabel
      Left = 21
      Top = 43
      Width = 60
      Height = 13
      Caption = #23384#25918#36335#24452#65306
    end
    object Label6: TLabel
      Left = 21
      Top = 71
      Width = 84
      Height = 13
      Caption = #36755#20837#25968#25454#24211#21517#65306
    end
    object RadioButton2: TRadioButton
      Left = 16
      Top = 97
      Width = 113
      Height = 17
      Caption = #26356#26032
      TabOrder = 4
      OnClick = RadioButton2Click
    end
    object RadioButton1: TRadioButton
      Left = 16
      Top = 18
      Width = 113
      Height = 17
      Caption = #21021#22987#21270
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = RadioButton1Click
    end
    object ComboBox1: TComboBox
      Left = 111
      Top = 117
      Width = 249
      Height = 21
      Style = csDropDownList
      Enabled = False
      TabOrder = 5
      OnChange = ComboBox1Change
    end
    object Edit5: TEdit
      Left = 111
      Top = 41
      Width = 217
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
    object Edit4: TEdit
      Left = 111
      Top = 68
      Width = 249
      Height = 21
      TabOrder = 3
    end
    object Button3: TButton
      Left = 327
      Top = 40
      Width = 33
      Height = 23
      Caption = '....'
      TabOrder = 2
      OnClick = Button3Click
    end
  end
  object Button4: TButton
    Left = 312
    Top = 367
    Width = 62
    Height = 25
    Caption = #36864#20986
    TabOrder = 3
    OnClick = Button4Click
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 397
    Width = 402
    Height = 19
    Panels = <
      item
        Text = #24403#21069#36827#24230#65306
        Width = 60
      end
      item
        Style = psOwnerDraw
        Width = 200
      end>
    OnDrawPanel = StatusBar1DrawPanel
  end
  object ADOQGetDB: TADOQuery
    Connection = ADOConnGetDB
    ParamCheck = False
    Parameters = <>
    Left = 166
    Top = 305
  end
  object ADOConnGetDB: TADOConnection
    LoginPrompt = False
    Left = 135
    Top = 305
  end
  object ADOConneExecSQL: TADOConnection
    LoginPrompt = False
    Left = 134
    Top = 343
  end
  object ADOQExecSQL: TADOQuery
    Connection = ADOConneExecSQL
    ParamCheck = False
    Parameters = <>
    Left = 168
    Top = 343
  end
end
