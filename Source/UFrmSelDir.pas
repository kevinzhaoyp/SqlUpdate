unit UFrmSelDir;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms, ActnList, Menus,
  ImgList, StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, System.ImageList,
  System.Actions, System.StrUtils;

type
  TFrmSelDir = class(TForm)
    ActionList1: TActionList;
    actionOK: TAction;
    Actioncancel: TAction;
    IL16: TImageList;
    tv1: TTreeView;
    btnionOK: TButton;
    btnActioncancel: TButton;
    pnl1: TPanel;   procedure ActioncancelExecute(Sender: TObject);
    procedure actionOKExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure tv1Expanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure tv1Change(Sender: TObject; Node: TTreeNode);
  private
    function GetPath(ANode: TTreeNode): string;
  public
    { Public declarations }
  end;

var
  FrmSelDir: TFrmSelDir;
  Fdir:string;//返回目录，带\
implementation

uses UFrmMain;

{$R *.dfm}

procedure TFrmSelDir.ActioncancelExecute(Sender: TObject);
begin
  self.ModalResult :=Mrcancel;
end;

procedure TFrmSelDir.actionOKExecute(Sender: TObject);
begin
  self.ModalResult:=Mrok;
end;

procedure TFrmSelDir.FormCreate(Sender: TObject);
var
  aNode: TTreeNode;
  aStr: string;
begin
  with FrmMain.ADOQGetDB do
  begin
    close;
    sql.Clear;
    sql.Add('execute master..xp_fixeddrives 0');
    open;
    tv1.Items.BeginUpdate;
    while not (IsEmpty or Eof) do
    begin
      aStr := Format('%s:  (%sMB)', [
        Fields[0].AsString, FormatFloat('#,##0', Fields[1].AsFloat)]);
      aNode := tv1.Items.Add(nil, aStr);
      aNode.ImageIndex := 0;
      aNode.SelectedIndex := 0;
      //aNode.EditText := false;
      tv1.Items.AddChildFirst(aNode, '');
      Next;
    end;
    tv1.Items.EndUpdate;
  end;
end;

function TFrmSelDir.GetPath(ANode: TTreeNode): string;
begin
  Result := '';
  while ANode <> nil do
  begin
    if ANode.Level = 0 then
      Result := LeftStr(ANode.Text,1) + ':\' + Result
    else
      Result := ANode.Text + '\' + Result;
    ANode := ANode.Parent;
  end;
end;

procedure TFrmSelDir.tv1Change(Sender: TObject; Node: TTreeNode);
begin
  self.Caption := Format('选择的目录[%s]', [GetPath(Node)]);
end;

procedure TFrmSelDir.tv1Expanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
var
  aStr: String;
  aNode: TTreeNode;
begin
  inherited;
  tv1.Items.BeginUpdate;
  try
    aStr := GetPath(Node);
    Node.DeleteChildren;
    with FrmMain.ADOQGetDB do
    begin
      close;
      sql.Clear;
      sql.Add('execute master..xp_dirtree ');
      sql.Add(QuotedStr(aStr) + ', 1, 0') ;
      open;
      while not (IsEmpty or Eof) do
      begin
        aStr := Format('%s', [Fields[0].AsString]);
        aNode := tv1.Items.AddChild(Node, aStr);
        aNode.ImageIndex := 0;
        aNode.SelectedIndex := 0;
        //aNode.EditText := false;
        tv1.Items.AddChildFirst(aNode, '');
        Next;
      end;
    end;
  finally
    tv1.Items.EndUpdate;
  end;
end;

end.
