unit UFrmMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, DB, ADODB, Gauges;

type
  TFrmMain = class(TForm)
    Button2: TButton;
    ADOQGetDB: TADOQuery;
    ADOConnGetDB: TADOConnection;
    ADOConneExecSQL: TADOConnection;
    ADOQExecSQL: TADOQuery;
    Gauge2: TGauge;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    GroupBox2: TGroupBox;
    RadioButton2: TRadioButton;
    RadioButton1: TRadioButton;
    ComboBox1: TComboBox;
    Edit5: TEdit;
    Edit4: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Button1: TButton;
    Button3: TButton;
    Button4: TButton;
    StatusBar1: TStatusBar;
    procedure Button1Click(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure StatusBar1DrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel;
      const Rect: TRect);
  private
    procedure searchfile(Astrs:TStrings;path:string);//取目录下的SQL文件到列表Astrs
    procedure Exsql(Afile:string);//执行sql文件
    procedure WriteLog(ErrStr:String);//写入日志文件

    Function  conndatabase(Adatabasename:string):boolean;//链接数据库
    procedure setselfcaption;//设置窗体标题
    Function  pcreatedatabase(Adatabasename,Apath:string):boolean;//创建数据库过程
  public
    Fconnmemo:string;//窗体标题显示连接数据库说明
    Fbarmemo:string;//在状态栏中显示执行文件个数。
    Fboolconn:boolean;//是否链接到服务器
    Ffilecount:integer;//待更新的文件总个数

    FfuncStrs: TStrings; //函数列表
    FInitStrs: TStrings; //初始化列表
    FprocStrs: TStrings; //过程列表
    FtableStrs: TStrings; //表列表
    FviewStrs: TStrings; //视图列表
    
  end;

var
  FrmMain: TFrmMain;

implementation

uses UFrmSelDir;

{$R *.dfm}
procedure TFrmMain.WriteLog(ErrStr:String); //写入日志文件
var
  LogFilename: String;
  LogFile: TextFile;
begin  
  LogFilename:=ExtractFilePath(ParamStr(0)) + 'LOG.LOG';
  AssignFile(LogFile, LogFilename);
  if FileExists(LogFilename) then Append(LogFile)
  else Rewrite(LogFile);
  Writeln(Logfile,DateTimeToStr(now)+': '+ErrStr);
  CloseFile(LogFile);
end;

procedure  TFrmMain.searchfile(Astrs:TStrings;path:string);//注意,path后面要有'\';
var  //取目录下的SQL文件到列表Astrs
  SearchRec:TSearchRec;
  found:integer;
begin
  Astrs.Clear;
  found:=FindFirst(path+'*.sql',faAnyFile,SearchRec);
  while found=0 do
  begin
    if (SearchRec.Name<>'.') and (SearchRec.Name<>'..') and
       (SearchRec.Attr<>faDirectory) then
    begin
       //ListBox1.Add(SearchRec.Name);
       Astrs.Append(path+SearchRec.Name);
    end;
    found:=FindNext(SearchRec);
  end;
  FindClose(SearchRec);
end;

procedure TFrmMain.setselfcaption; //设置窗体标题栏
begin
  if radiobutton1.Checked then
    self.Caption:= radiobutton1.Caption +Fconnmemo
  else
    self.Caption:= radiobutton2.Caption +Fconnmemo;
end;

procedure TFrmMain.StatusBar1DrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel;
  const Rect: TRect);
begin  //让进度条显示在状态栏上
  if Panel = StatusBar.Panels[1] then
  with Gauge2 do begin
    Top := Rect.Top+2;
    Left := Rect.Left;
    Width := Rect.Right - Rect.Left-2;
    Height := Rect.Bottom - Rect.Top-4;
    BackColor :=StatusBar.Color;
  end;
end;

procedure TFrmMain.Button1Click(Sender: TObject);
var
  i : integer;
begin
  FfuncStrs:= TStringList.Create; //函数列表
  FInitStrs:= TStringList.Create; //初始化列表
  FprocStrs:= TStringList.Create; //过程列表
  FtableStrs:= TStringList.Create; //表列表
  FviewStrs:= TStringList.Create; //视图列表
  combobox1.Clear;
  if ADOConnGetDB.Connected then ADOConnGetDB.Close;
  ADOConnGetDB.ConnectionString := 'Provider=SQLOLEDB.1;Persist Security Info=True;;'+
                                   'Password=' + Trim(edit3.Text) +
                                   ';User ID=' + Trim(edit2.Text) +
                                   ';Data Source=' + Trim(edit1.Text);
  try
    ADOConnGetDB.Connected := True;
    showmessage('链接成功。');
    Fconnmemo:='[链接成功：'+edit1.text+'->'+edit2.text+'->'+'Master]';
    setselfcaption;
    button2.Enabled :=true;
    Fboolconn:=true;
  except
    showmessage('链接失败。');
    Fconnmemo:='[链接失败：'+edit1.text+'->'+edit2.text+'->'+'Master]';
    setselfcaption;
    Fboolconn:=false;
    exit;
  end;
  with ADOQGetDB do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'select name from master..sysdatabases order by name';
    Open;
    first;
    for i := 1 to RecordCount do
    begin
      combobox1.Items.Add(FieldByName('name').AsString);
      Next;
    end;
  end;
  searchfile(FfuncStrs,ExtractFilePath(ParamStr(0))+'func\');
  searchfile(FInitStrs,ExtractFilePath(ParamStr(0))+'Init\');
  searchfile(FprocStrs,ExtractFilePath(ParamStr(0))+'proc\');
  searchfile(FtableStrs,ExtractFilePath(ParamStr(0))+'table\');
  //FviewStrs.LoadFromFile(ExtractFilePath(ParamStr(0))+'view\Vie.txt');
  //searchfile(FviewStrs,ExtractFilePath(ParamStr(0))+'view\');
end;

procedure TFrmMain.Button2Click(Sender: TObject);
var
  i:integer;
begin
  Ffilecount:=FInitStrs.Count+FfuncStrs.Count+ FprocStrs.Count+ FtableStrs.Count
      +FviewStrs.Count;
  Gauge2.MaxValue :=Ffilecount;
  Gauge2.Progress :=0;
  if radiobutton1.Checked  then
    if not pcreatedatabase(trim(edit4.text),trim(edit5.text)) then
      exit;//创建数据库。
  WriteLog('-----------------表-------------------');
  for I := 0 to FtableStrs.Count - 1 do Exsql(FtableStrs.Strings[i]);

  WriteLog('-----------------函数-----------------');
  for I := 0 to FfuncStrs.Count - 1 do Exsql(FfuncStrs.Strings[i]);

  WriteLog('-----------------视图-----------------');
  for I := 0 to FviewStrs.Count - 1 do
    Exsql(ExtractFilePath(ParamStr(0))+'view\' + FviewStrs.Strings[i]);

  WriteLog('-----------------过程-----------------');
  for I := 0 to FprocStrs.Count - 1 do Exsql(FprocStrs.Strings[i]);

  WriteLog('-----------------初始化---------------');
  for I := 0 to FInitStrs.Count - 1 do Exsql(FInitStrs.Strings[i]);

  self.Caption :='更新完毕';
  WriteLog('更新完毕');
  showmessage('更新完毕');
end;

procedure TFrmMain.Button3Click(Sender: TObject);
begin
 { if SelectDirectory('Select a directory','',direct) then
  begin    选择本地目录   加单元FileCtrl
    if direct[length(direct)]<>'\' then direct:=direct+'\';
    edit5.Text:=direct;
  end; }
  if not Fboolconn then
  begin
    showmessage('请先链接到数据库');
    exit;
  end;
  Application.CreateForm(TFrmSelDir, FrmSelDir);
  if FrmSelDir.ShowModal=Mrok then
    edit5.text:=Fdir
  else
    edit5.Clear;
  FrmSelDir.free;
end;

procedure TFrmMain.Button4Click(Sender: TObject);
begin
  close;
end;

procedure TFrmMain.ComboBox1Change(Sender: TObject);
begin  //
  conndatabase(trim(combobox1.Text));
end;

Function TFrmMain.conndatabase(Adatabasename: string):boolean;
begin
  result:=true;
  try
    if ADOConneExecSQL.Connected then ADOConneExecSQL.Connected := False;

    ADOConneExecSQL.ConnectionString := 'Provider=SQLOLEDB.1;Persist Security Info=True;;'+
                                        'Password=' + Trim(edit3.Text) +
                                        ';User ID=' + Trim(edit2.Text) +
                                        ';Initial Catalog=' + Adatabasename +
                                        ';Data Source=' + Trim(edit1.Text);
    ADOConneExecSQL.Connected := True;
    Fconnmemo:='[链接成功：'+edit1.text+'->'+edit2.text+'->'+Adatabasename+']';
    setselfcaption;
  except
    MessageBox(Handle,Pchar('连接数据库 ' + Adatabasename +' 失败!'),
        '提示',MB_OK);
    Fconnmemo:='[链接成功：'+edit1.text+'->'+edit2.text+'->'+Adatabasename+']';
    setselfcaption;
    result :=false;
  end
end;

procedure TFrmMain.Exsql(Afile: string);
var
  i:integer;
  Ffilelist:Tstringlist;
  Fgo:boolean;//判断有没有执行过语句
begin
  if (Trim(Afile) = '') or (not FileExists(Trim(Afile))) then
  begin
    WriteLog(ExtractFileName(Afile)+'文件错误');;
    Exit;
  end;
  if not ADOConneExecSQL.Connected then
  begin
    MessageBox(Handle,Pchar('请选择要连接的数据库!'),'提示',MB_OK);
    Exit;
  end;
  Ffilelist:=Tstringlist.Create;
  Ffilelist.LoadFromFile(Trim(Afile));
  self.Caption :='正在更新：'+ExtractFileName(Afile);
  ADOConneExecSQL.BeginTrans;
  ADOQExecSQL.SQL.Clear;
  Fgo:=false;
  try
    for i := 0 to Ffilelist.Count - 1 do
    begin
      application.ProcessMessages;//让窗体接受事件
      if trim(Ffilelist.Strings[i])='' then continue; //如果是空行就进行下一次循环
      if uppercase(trim(Ffilelist.Strings[i]))='GO' then
      begin      //如果是'GO'就执行sql语句。
        //showmessage(ADOQExecSQL.SQL.Text );
        ADOQExecSQL.ExecSQL;
        ADOQExecSQL.Close;
        ADOQExecSQL.SQL.Clear;
        Fgo:=true;
      end
      else
      begin
        ADOQExecSQL.SQL.Add(Ffilelist.Strings[i]);
      end;
    end;
    ADOConneExecSQL.CommitTrans;
    Ffilelist.Free;
    if Fgo then
      WriteLog(ExtractFileName(Afile)+' 更新成功')
    else
      WriteLog(ExtractFileName(Afile)+' 空文件。');
  except
    Ffilelist.Free;
    WriteLog(ExtractFileName(Afile)+' 更新失败');
    ADOConneExecSQL.RollbackTrans;
  end;
  Gauge2.Progress :=Gauge2.Progress +1;
end;

procedure TFrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ADOConnGetDB.Close;
  ADOConneExecSQL.Close;
end;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
  Gauge2.Parent:=StatusBar1;
end;

Function TFrmMain.pcreatedatabase(Adatabasename,Apath:string):boolean;
begin   //创建数据库 传入数据库名和保存路径。
  result:=false;
  if (Adatabasename='') or (Apath='') then
  begin
    showmessage('请选择数据库存放路径，并且输入数据库名');
    exit;
  end;
  try
    with ADOQGetDB do
    begin
      Close;
      SQL.Clear;
      SQL.Text := 'USE master';
      ExecSQL;
      
      close;
      sql.clear;
      sql.Add('CREATE DATABASE '+Adatabasename);
      sql.Add('ON');
      sql.add('( NAME = '+Adatabasename+'_dat,');
      sql.Add('FILENAME = '''+Apath+Adatabasename+'.mdf'',');
      sql.Add('SIZE = 10,MAXSIZE = UNLIMITED ,FILEGROWTH = 5 )');
      sql.add('LOG ON');
      sql.Add('( NAME = '''+Adatabasename+'_log'',');
      sql.Add('FILENAME = '''+Apath+Adatabasename+'.ldf'',');
      sql.Add('SIZE = 5MB,MAXSIZE = UNLIMITED ,FILEGROWTH = 5MB)');
      //showmessage(sql.Text );
      ExecSQL;
    end;
    WriteLog('数据库'+Adatabasename+'创建成功。');
  except
    WriteLog('数据库'+Adatabasename+'创建失败。');
  end;
  if conndatabase(Adatabasename)then //链接数据库
    result:=true;
end;

procedure TFrmMain.RadioButton1Click(Sender: TObject);
begin
  //初始化
  edit4.enabled := radiobutton1.Checked ;
  edit5.enabled := radiobutton1.Checked ;
  button3.Enabled :=radiobutton1.Checked ;
  combobox1.enabled :=not radiobutton1.Checked ;

  button2.caption:=radiobutton1.Caption;
  setselfcaption;
end;

procedure TFrmMain.RadioButton2Click(Sender: TObject);
begin
  //更新
  edit4.enabled :=not radiobutton2.Checked ;
  edit5.enabled :=not radiobutton2.Checked ;
  button3.Enabled :=not radiobutton2.Checked ;
  combobox1.enabled := radiobutton2.Checked ;
  
  button2.Caption := radiobutton2.Caption;
  setselfcaption;
end;

end.
